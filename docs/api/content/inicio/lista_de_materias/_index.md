---
title: "Lista de Materias"
date: 2019-12-03T15:52:46-03:00
weight: 3
---

+ 1 RC servo - tamanho normal
+ 1 RC servo (alterado para girar 360°) 
+ 1 Broca impressa na Impressora 3D
+ 1 Agitador impresso na Impressora 3D
+ 1 Cano PVC 'T' 1/2"
+ 1 Arduino Uno
+ 1 Encoder KY-040 
+ 1 Display LCD HD44780
+ 1 Botão
+ 1 Funil de alimentos
+ 1 Caixa de MDF
+ 1 Fonte 12V (500Ma)
+ 2 Capacitores eletrolíticos 25µ 

{{% notice info %}}
As peças impressas foram cedidas pela Robotinik.
{{% /notice %}}   

