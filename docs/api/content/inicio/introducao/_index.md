---
title: "Introducao"
date: 2019-12-03T15:51:36-03:00
weight: 1
---

Descrição do Projeto:
Trata-se de um alimentador automático para animais domésticos de pequeno porte (podendo - o projeto - ser alterado de acordo com a necessidade), que armazena a ração em um recipiente e libera a quantia desejada, a cada período de tempo (horas) programada, de modo que a ração permaneça sempre limpa e fresca. Evita-se desperdício e eventuais problemas pela contaminação do alimento.
