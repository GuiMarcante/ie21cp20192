---
title: "Projeto Base"
date: 2019-12-03T15:52:05-03:00
weight: 2
---

Este projeto foi feito com intuito acadêmico e para uso pessoal, baseado no projeto original do usuário @dodgey99
do Instructables, e com algumas alterações para melhorias.

{{% notice info %}}
O projeto original pode ser encontrado neste [LINK](https://www.instructables.com/id/Automatic-Arduino-Powered-Pet-Feeder/ "Projeto original")!
{{% /notice %}}





