---
title: "Home"
date: 2019-12-02T17:08:59-03:00
---

# Projeto "Gatomático"

## Participantes:

+ ##### Bruno de M. R. Guerreiro - RA: 2198762
+ ##### Guilherme Iago M. D. Libera - RA: 2199572
+ ##### Dhayane Khellem T. Lütkenhaus -   RA: 670316 

**Projeto realizado como avaliação para a disciplina de "Introdução a engenharia" na UTFPR-PB, no periodo 2019-2.**

- Professor responsável: Prof. Me. Jeferson José de Lima 

- Contato: jefersonlima@utfpr.edu.br

---
## Contato:

bmguerreiro2012@gmail.com

guimarcantee@gmail.com

dhayane@alunos.utfpr.edu.br
