# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação do 1º período na UTFPR - PB:

|Nome| gitlab user|
|---|---|
|Guilherme Iago Marcante Della Libera|@GuiMarcante|
|Dhayane Khellem Toldo Lütenhaus|@khellem|
|Bruno de Macedo Ribeiro Guerreiro|@BrunoGuerreiro08|	
|Jeferson Lima| @jeferson.lima|

# Documentação

A documentação do projeto pode ser acessada pelo link:

> https://guimarcante.gitlab.io/ie21cp20192/

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)
